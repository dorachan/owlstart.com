$(document).ready(function(){

    $(window).resize(function() {
        location.reload(true);
    });

    (function(){
        var time = 700,
        timer;

        function fadeIn(){
            clearTimeout(timer);
            $(".hides").stop(true).fadeIn(80);
        }
        function fadeOut(){
            timer = setTimeout(function(){
                $(".hides").fadeOut();
            }, time);
        }
        $(".project").hover(fadeIn, fadeOut);
    }());

    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    $(".container").css({height: windowHeight});
    $(".scene").css({height: windowHeight*0.5, width: windowWidth*0.5, marginTop: windowHeight*0.2});
    $("#scene").parallax();


    $(".stars").css({top: windowHeight*0.17, left: windowWidth*0.17});

    circle_radius = windowHeight*0.25;
    $links = $('.circle');
    total_links = $links.size();
    $links.each(function(index) {
        $(this).attr('data-index',index);
        animateCircle($(this), 1);
    });
    $('.circle').hover(function() {
        animateCircle($(this), 1.2);
    }, function() {
        animateCircle($(this), 1);
    });

    function animateCircle($link, expansion_scale) {
        index = $link.attr('data-index');
        radians = 2*Math.PI*(index/total_links);
        x = -(Math.sin(radians)*circle_radius*expansion_scale);
        y = -(Math.cos(radians)*circle_radius*expansion_scale);
        $link.animate({
            top: x+'px',
            left: y+'px'
        }, 300);
    }

    $(".stars a").nivoLightbox({
        keyboardNav: true
    });

});
